﻿# Weather Dashboard: Washboard

A skin for the [weewx](http://weewx.com/) weather system.  It utilizes [Bootstrap](https://getbootstrap.com/), and [chartist.js](https://gionkunz.github.io/chartist-js/) to render a mobile friendly weather webpage.

[Taking a look at a demo](http://jquagga.gitlab.io/Washboard/) will likely give you a sense of what this skin does far better than what I could write here.  It was built around the concept that the primary user of this weather data is you, the weather station owner.  It replaces the weewx generated images with chartist graphs which are bigger and hopefully easier to read.  With bootstrap, the webpage works on a phone, tablet, or computer.

At this point, I've been using this skin as my daily driver for months.  It's not perfect but I'm very happy with it.  It's functional and usable on everything from a phone to a laptop (mostly thanks to bootstrap).

## Getting Started

Generally speaking, all you have to do is download a copy of this repository to your weewx/skins directory, change your skin in weewx.conf, and then restart weewx.

### Prerequisites

What things you need to install the software and how to install them

* A functional weewx installation

### Installing

Assuming weewx is in /etc/weewx
```bash
cd /etc/weewx/skins
git clone https://gitlab.com/jquagga/Washboard
```

Then edit /etc/weewx/weewx.conf to change the skin varible under:

```python
[[StandardReport]]
skin = Washboard
```

Finally restart Weewx

#### Configuring additional options
Now that you have the skin up and running, you may want to enable a few other things.  

There are two options at the top of skin.conf.  One enables a link in the menu to the NOAA NWS forecast for the station location of your weatherstation.  I believe it should work for most areas NOAA creates a forecast.  It's relatively lightweight.

The other option is noaa_radar.  That will enable a leaflet.js OpenStreetMap of your station's location and overlay the NOAA CONUS radar over it.  This is limited to the contiguous US and it adds a bit more javascript to the index page.  But it gives you a nice radar view and you can click on it to get a full screen zoomable / panable radar view.  

## Built With

* [Bootstrap](https://getbootstrap.com/)- The web framework used
* [Chartist.js](https://gionkunz.github.io/chartist-js/) - Javascript library rendering the charts
* [weewx](http://weewx.com/) - Weather Station software making this possible
* [leaflet.js](https://leafletjs.com/) - Drives the radar map

## License

This project is licensed under the GPLv3 License - see the [LICENSE](LICENSE) file for details.  Generally I'm more of a MIT License guy at heart, but I built this on a lot of GPLv3 code so I just went with that.

It's a web template.  Take it and make it your own!
